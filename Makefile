CC := g++
FLAGS := -std=c++11 -Wall
EXE := main
TESTEXE := tests
OBJECTS := main.o Matrix.o MatrixAlgorithms.o
TESTOBJECTS := Tests.o Matrix.o

build: $(OBJECTS)
	$(CC) $(FLAGS) $(OBJECTS) -o $(EXE)

test: $(TESTOBJECTS)
	$(CC) $(FLAGS) $(TESTOBJECTS) -o $(TESTEXE)
	./$(TESTEXE)

main.o: main.cpp
	$(CC) $(FLAGS) -c main.cpp

Tests.o: Tests.cpp Tests.hpp Matrix.o
	$(CC) $(FLAGS) -c Tests.cpp

Matrix.o: Matrix.cpp Matrix.hpp
	$(CC) $(FLAGS) -c Matrix.cpp

MatrixAlgorithms.o: Matrix.o MatrixAlgorithms.cpp MatrixAlgorithms.hpp
	$(CC) $(FLAGS) -c MatrixAlgorithms.cpp

run:
	./$(EXE)

clean:
	rm *.o $(EXE)
