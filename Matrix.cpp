#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>

#include "Matrix.hpp"

/**
 * Defines a method used to initialise variables for the Matrix
 * @param Height The height of the Matrix
 * @param Width  The width of the Matrix
 */
void Matrix::Initialise(int Height, int Width) {
    this->Height = Height;
    this->Width = Width;
    this->Elements = this->Height * this->Width;
    this->Data = new double[Elements];
}

/**
 * Defines a method to check that both rows of a matrix exists when trying to swap them
 * @param a The initial row
 * @param b The row you are swapping it with
 */
void Matrix::CheckRowsExist(int a, int b) {
    if (a < 0 || Height <= a || b < 0 || Height <= b) {
        throw std::invalid_argument("Cannot swap rows that do not exist.");
    }
}

/**
 * Defines a swap method used to swap two elements of the Matrix without the constant need
 * for temporary variables
 * @param a The row of the first element
 * @param b The column of the first element
 * @param c The row of the second element
 * @param d The column of the second element
 */
void Matrix::Swap(int a, int b, int c, int d) {
    double Temp = Get(a, b);
    Set(a, b, Get(c, d));
    Set(c, d, Temp);
}

/**
 * Defines a constructor for the Matrix that takes a Height and Width and sets
 * all values to 0 in the Matrix
 * @param Height The height of the matrix to construct
 * @param Width  The width of the matrix to construct
 */
Matrix::Matrix(int Height, int Width) {
    Initialise(Height, Width);
    SetAll(0.0);
}

/**
 * Defines a constructor for the Matrix that takes a Height, Width and a Value, resulting
 * in all values in the resulting Matrix initialised to Value
 * @param Height The height of the matrix
 * @param Width  The width of the matrix
 * @param Value  The value to initialise all elements to
 */
Matrix::Matrix(int Height, int Width, double Value) {
    Initialise(Height, Width);
    SetAll(Value);
}

/**
 * Defines a constructor for the Matrix that takes a Height, Width and a pointer to a
 * double (expected to be an array) to initialise the Matrix
 * @param Height The height of the matrix
 * @param Width  The width of the matrix
 * @param Input  A pointer to an array to set the Matrix to
 */
Matrix::Matrix(int Height, int Width, double* Input) {
    Initialise(Height, Width);
    SetFromArray(Input);
}

/**
 * Defines a deconstructor for the Matrix that deletes the Data and the pointers to
 * Pivot, Upper and Lower
 */
Matrix::~Matrix() {
    delete[] this->Data;
}

/**
 * Defines a Get method to get a single element of the Matrix
 * @param  row The column to get
 * @param  col The row to get
 * @return     The value at that index
 */
double Matrix::Get(int row, int col) const {
    return Data[row * Width + col];
}

/**
 * Defines a Set method to set the value of a specific index of the Matrix
 * @param row The row to set
 * @param col The column to set
 * @param val The value to set it to
 */
void Matrix::Set(int row, int col, double val) {
    Data[row * Width + col] = val;
}

/**
 * Defines a method to set all of the values of the Matrix to a given value
 * @param Value The value to set all values to
 */
void Matrix::SetAll(double Value) {
    for (int i = 0; i < Elements; ++i) {
        Data[i] = Value;
    }
}

/**
 * Defines a method to set all the values of a Matrix from an array of doubles
 * @param Input A pointer to a double (assumed to be a pointer to an array of doubles)
 */
void Matrix::SetFromArray(double* Input) {
    for (int i = 0; i < Elements; ++i) {
        Data[i] = Input[i];
    }
}

/**
 * Defines a Get method for the Height of the Matrix
 * @return The height of the matrix
 */
int Matrix::GetHeight() const {
    return this->Height;
}

/**
 * Defines a Get method for the Width of the Matrix
 * @return The width of the matrix
 */
int Matrix::GetWidth() const {
    return this->Width;
}

/**
 * Defines a method used to swap the rows of a Matrix
 * @param a The initial row
 * @param b The row to swap it with
 */
void Matrix::SwapRows(int a, int b) {
    CheckRowsExist(a, b);

    for (int i = 0; i < Width; ++i) {
        Swap(a, i, b, i);
    }
}

/**
 * Defines a method to swap the columns of a Matrix
 * @param a The initial column
 * @param b The column to swap it with
 */
void Matrix::SwapColumns(int a, int b) {
    if (a < 0 || Height <= a || b < 0 || Height <= b) {
        throw std::invalid_argument("Cannot swap columns that do not exist.");
    }

    for (int i = 0; i < Width; ++i) {
        Swap(i, a, i, b);
    }
}

/**
 * Defines a method to swap the subdiagonal rows of a Matrix, i.e the parts of the
 * row that appear below the diagonal
 * @param a The initial row
 * @param b The row to swap it with
 */
void Matrix::SwapSubdiagonalRows(int a, int b) {
    CheckRowsExist(a, b);

    for (int i = 0; i < std::min(a, b); ++i) {
        Swap(a, i, b, i);
    }
}

/**
 * Defines a method to swap the superdiagonal rows of a Matrix, i.e the parts of the
 * row that appear above the diagonal
 * @param a The initial row
 * @param b The row to swap it with
 */
void Matrix::SwapSuperdiagonalRows(int a, int b) {
    CheckRowsExist(a, b);

    for (int i = Width - 1; std::max(a, b) < i; i--) {
        Swap(a, i, b, i);
    }
}

/**
 * Defines a method used to copy the elements of the current Matrix into a new
 * matrix and return it
 * @return A Matrix with a copy of the elements within the instance one
 */
Matrix* Matrix::Copy() const {
    Matrix* Copied = new Matrix(Height, Width);
    for (int x = 0; x < Height; ++x) {
        for (int y = 0; y < Width; ++y) {
            Copied->Set(x, y, Get(x, y));
        }
    }
    return Copied;
}

/**
 * Defines a Display function for a Matrix to output it to the screen, displays with 2 digits
 * of precision
 */
void Matrix::Display() {
    std::streamsize ss = std::cout.precision();
    std::cout << '\n';
    for (int x = 0; x < Height; ++x) {
        for (int y = 0; y < Width; ++y) {
            std::cout << '\t' << std::setprecision(2) << Data[x * Width + y];
        }
        std::cout << '\n';
    }
    std::cout << '\n' << std::setprecision(ss);
}

/**
 * Defines an override of the addition operation on matrices, simply returning an addition of the
 * elements of the two matrices provided they have the same dimensions
 * @param   M The matrix to add to the current one
 * @return  The addition of the two matrices
 */
Matrix Matrix::operator+(const Matrix& M) {
    if (Height != M.Height || Width != M.Width) {
        throw std::invalid_argument("Cannot add the matrices as they have different sizes.");
    }

    Matrix Output(Height, Width);

    for (int x = 0; x < Height; ++x) {
        for (int y = 0; y < Width; ++y) {
            Output.Set(x, y, Get(x, y) + M.Get(x, y));
        }
    }

    return Output;
}

/**
 * Defines an override of the subtraction operation on matrices, simply returning a subtraction of the
 * elements of the two matrices provided they have the same dimensions
 * @param   M The matrix to subtract from the current one
 * @return  The subtraction of the two matrices
 */
Matrix Matrix::operator-(const Matrix& M) {
    if (Height != M.Height || Width != M.Width) {
        throw std::invalid_argument("Cannot subtract the matrices as they have different sizes.");
    }

    Matrix Output(Height, Width);

    for (int x = 0; x < Height; ++x) {
        for (int y = 0; y < Width; ++y) {
            Output.Set(x, y, Get(x, y) - M.Get(x, y));
        }
    }

    return Output;
}

/**
 * Defines an override of the multiplication operation on matrices, simply returning naive multiplication
 * of the two matrices provided they have valid dimensions
 * @param   M The matrix to multiply the current one by
 * @return  The multiplication of the two matrices
 */
Matrix Matrix::operator*(const Matrix& M) {
    if (Width != M.Height) {
        std::stringstream ErrorMessage;
        ErrorMessage << "Cannot multiply the matrices together. Current matrix is "
		<< Height << "*" << Width << " and input matrix is "
		<< M.Height << "*" << M.Width << ".";
        throw std::invalid_argument(ErrorMessage.str());
    }

    Matrix Output(Height, M.Width);

    for (int x = 0; x < Height; ++x) {
        for (int y = 0; y < M.Width; ++y) {
            double Sum = 0.0;

            for (int z = 0; z < Width; ++z) {
                Sum += Get(x, z) * M.Get(z, y);
            }

            Output.Set(x, y, Sum);
        }
    }

    return Output;
}

Matrix Matrix::operator*(const double& Scale) {
    Matrix Output(Height, Width);

    for (int x = 0; x < Height; ++x) {
        for (int y = 0; y < Width; ++y) {
            Output(x, y) = Scale * Get(x, y);
        }
    }

    return Output;
}

double Matrix::operator()(int row, int col) const {
    return Data[row * Width + col];
}

double& Matrix::operator()(int row, int col) {
    return Data[row * Width + col];
}

bool Matrix::operator==(const Matrix& M) const {
    if (Height != M.Height || Width != M.Width) return false;

    for (int i = 0; i < Height; ++i) {
        for (int j = 0; j < Width; ++j) {
            if (M.Get(i, j) != Get(i, j)) return false;
        }
    }

    return true;
}

bool Matrix::operator==(const double* Input) const {
    for (int i = 0; i < Elements; ++i) {
        if (Data[i] != Input[i]) return false;
    }
    return true;
}

bool Matrix::operator!=(const Matrix& M) const {
    return !((*this) == M);
}

bool Matrix::operator!=(const double* Input) const {
    return !((*this) == Input);
}
