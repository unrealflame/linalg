#ifndef MATRIX_HPP
#define MATRIX_HPP

class Matrix {
    private:
        int Height, Width, Elements;
        double *Data;

        void Initialise(int, int);
        void CheckRowsExist(int, int);

    public:
        Matrix(int, int);
        Matrix(int, int, double);
        Matrix(int, int, double*);
        virtual ~Matrix();

        double Get(int, int) const;
        void Set(int, int, double);
        void SetAll(double);
        void SetFromArray(double*);
        int GetHeight() const;
        int GetWidth() const;

        void Swap(int, int, int, int);
        void SwapRows(int, int);
        void SwapColumns(int, int);
        void SwapSubdiagonalRows(int, int);
        void SwapSuperdiagonalRows(int, int);

        Matrix* Copy() const;
        void Display();

        Matrix operator+(const Matrix&);
        Matrix operator-(const Matrix&);
        Matrix operator*(const Matrix&);
        Matrix operator*(const double&);
        double operator()(int, int) const;
        double& operator()(int, int);
        bool operator==(const Matrix&) const;
        bool operator==(const double*) const;
        bool operator!=(const Matrix&) const;
        bool operator!=(const double*) const;
};

#endif
