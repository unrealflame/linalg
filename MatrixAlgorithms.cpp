#include <iostream>
#include <cmath>

#include "MatrixAlgorithms.hpp"

Decomposition Decompose(const Matrix& M) {
    int Height = M.GetHeight(), Width = M.GetWidth();
    Matrix *Upper = M.Copy();
    Matrix *Lower = new Matrix(Height, Width);
    Matrix *Pivot = new Matrix(Height, Width);
    int PivotSwaps = 0;

    // Set the diagonals of Pivot and Lower
    for (int i = 0; i < Height; ++i) {
        Pivot->Set(i, i, 1);
        Lower->Set(i, i, 1);
    }

    // For each column
    for (int c = 0; c < Width - 1; ++c) {
        // Find the row index of the maximum element of that column
        int MaxRow = c;
        for (int r = c + 1; r < Height; ++r) {
            if (fabs(Upper->Get(MaxRow, c)) < fabs(Upper->Get(r, c))) {
                MaxRow = r;
            }
        }

        // Swap the rows of this matrix and the pivot provided they aren't the same rows
        if (MaxRow != c) {
            Upper->SwapRows(MaxRow, c);
            Lower->SwapSubdiagonalRows(MaxRow, c);
            Pivot->SwapRows(MaxRow, c);
            PivotSwaps++;
        }

        // Now we can perform Gaussian elimination
        for (int r = c + 1; r < Height; ++r) {
            double Factor = -(Upper->Get(r, c) / Upper->Get(c, c));
            for (int col = 0; col < Width; ++col) {
                Upper->Set(r, col, Factor * Upper->Get(c, col) + Upper->Get(r, col));
            }
            Lower->Set(r, c, -Factor);
        }
    }

    Decomposition Decomp(Lower, Upper, Pivot, PivotSwaps);
    return Decomp;
}

double Determinant(const Matrix& M) {
    Decomposition Decomp = Decompose(M);
    return Determinant(Decomp);
}

double Determinant(const Decomposition& Decomp) {
    double UpperDet = 1.f, LowerDet = 1.f;

    for (int i = 0; i < Decomp.Upper->GetHeight(); ++i) {
        UpperDet *= Decomp.Upper->Get(i, i);
        LowerDet *= Decomp.Lower->Get(i, i);
    }
    
    return pow(-1, Decomp.PivotSwaps) * UpperDet * LowerDet;
}

Matrix Transpose(const Matrix& M) {
    int Height = M.GetHeight(), Width = M.GetWidth();
    Matrix Transposed(Width, Height);

    for (int i = 0; i < Height; ++i) {
        for (int j = 0; j < Width; ++j) {
            Transposed(j, i) = M(i, j);
        }
    }

    return Transposed;
}
