#ifndef MATRIXALGORITHMS_H
#define MATRIXALGORITHMS_H

#include "Matrix.hpp"

struct Decomposition {
    Matrix *Lower, *Upper, *Pivot;
    int PivotSwaps;

    Decomposition(Matrix* L, Matrix* U, Matrix* P, int Swaps) :
    Lower(L), Upper(U), Pivot(P), PivotSwaps(Swaps) {};
};

Decomposition Decompose(const Matrix&);
double Determinant(const Matrix&);
double Determinant(const Decomposition&);
Matrix Transpose(const Matrix&);

#endif
