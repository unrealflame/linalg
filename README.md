# C++ Matrices

Contains an implementation of Matrices in C++ with multiplication, decomposition and finding the determinants of them.

## Prerequisites

Requires C++11

## Installation

Cloning the project and running the below command should build the project into "main" and run the code

```
make build run
```

## Testing

When run for the first time, the program should output -35 and 8 (just tests)
