#include <iostream>

#include "Matrix.hpp"
#include "Tests.hpp"

int main(int argc, char const *argv[]) {
    RunTests();
    return 0;
}

void RunTests() {
    TestAddition();
    TestSubtraction();
    TestMultiplication();

    std::cout << "All tests passed :)" << '\n';
}

void TestAddition() {
    double arr[] = {1, 2, 3, 4};
    double brr[] = {2, 3, 4, 5};
    double Result[] = {3, 5, 7, 9};
    Matrix A(2, 2, arr);
    Matrix B(2, 2, brr);

    if (!(A + B == Result)) throw std::logic_error("Addition has definitely broken.");
    std::cout << "Matrix Addition works :)" << '\n';
}

void TestSubtraction() {
    double arr[] = {1, 2, 3, 4};
    double brr[] = {2, 3, 4, 5};
    double Result[] = {-1, -1, -1, -1};
    Matrix A(2, 2, arr);
    Matrix B(2, 2, brr);

    if (!(A - B == Result)) throw std::logic_error("Subtraction has definitely broken.");
    std::cout << "Matrix Subtraction works :)" << '\n';
}

void TestMultiplication() {
    double arr[] = {1, 2, 3, 4};
    double brr[] = {2, 3, 4, 5};
    double ResultAB[] = {10, 13, 22, 29};
    double ResultBA[] = {11, 16, 19, 28};
    Matrix A(2, 2, arr);
    Matrix B(2, 2, brr);

    if (!(A * B == ResultAB) || !(B * A == ResultBA)) {
        throw std::logic_error("Multipication has definitely broken for n*n matrices");
    }
    std::cout << "Matrix Multipication works for n*n :)" << '\n';

    double crr[] = {1, 2, 3};
    double drr[] = {2, 3, 4};
    double ResultCD[] = {20};
    double ResultDC[] = {2, 4, 6, 3, 6, 9, 4, 8, 12};
    Matrix C(1, 3, crr);
    Matrix D(3, 1, drr);
    if (!(C * D == ResultCD) || !(D * C == ResultDC)) {
        throw std::logic_error("Multipication has definitely broken for non n*n matrices");
    }
    std::cout << "Matrix Multipication works for non n*n :)" << '\n';
}
