#include <iostream>
#include <chrono>

#include "Matrix.hpp"
#include "MatrixAlgorithms.hpp"

int main(int argc, char const *argv[]) {
    srand(time(NULL));

    Matrix A(3, 3, 1);
    std::cout << Determinant(A) << '\n';

    double arr[] = {0, 0, 1, 4, 7, 2, 5, 0, 9};
    Matrix M(3, 3, arr);
    // M.RunTests();

    double brr[] = {2, 1, 1, 0, 4, 3, 3, 1, 8, 7, 9, 5, 6, 7, 9, 8};
    Matrix B(4, 4, brr);
    // B.RunTests();
    Decomposition D = Decompose(B);
    std::cout << Determinant(D) << '\n';
    std::cout << Determinant(B) << '\n';
    Transpose(B).Display();

    return 0;
}
